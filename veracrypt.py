import requests
import os
import gnupg
import logging

VERACRYPT_VERSION = "1.24"


class PackageManagement(object):
    """
    Class for package management
    """
    @staticmethod
    def download(url, name):
        """
        downloading package from URL
        :param url: url of the package
        :param name: name to save the package
        """
        try:
            download_package = requests.get(url)
            if download_package.status_code == 200:
                with open('/tmp/{0}'.format(name), 'wb') as download_file:
                    download_file.write(download_package.content)
        except Exception as e:
            logging.error("Package couldn't be downloaded")
            print(e)


    def verify_package(self, package, signature):
        """
        Verify package GPG signature
        :param package: path to package to verify
        :param signature: signature file to verify against
        :return: returns True upon correct verification or False on failure
        """
        gpg = gnupg.GPG(gnupghome="{}/.gnupg/".format(os.path.expanduser("~")))
        signature = open(signature, "rb")
        verified = gpg.verify_file(signature, package)
        return verified

class Veracrypt(PackageManagement):
    """
    Class for interaction with Veracrypt
    """

    def verify_veracrypt(self, package, signature):
        """
        Verify package PGP signature
        :param package: package path
        :param signature: signature of the package
        :returns True or False
        """
        veracrypt_pub_fingerprint = "5069A233D55A0EEB174A5FC3821ACD02680D16DE"
        gpg = gnupg.GPG(gnupghome="{}/.gnupg/".format(os.path.expanduser("~")))  # defaults to home folder of the user
        self.download("https://www.idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key.asc", "veracrypt.asc")
        if gpg.scan_keys("/tmp/veracrypt.asc").fingerprints[0] == veracrypt_pub_fingerprint:
            key = open("/tmp/veracrypt.asc", "r")
            gpg.import_keys(key.read())
            return self.verify_package(package, signature)
        else:  # if the key's fingerprint doesn't match than False is returned
            logging.error("Fingerprint of the downloaded key is incorrect")
            return False

    def download_veracrypt(self):
        """
        Download and verify Veracrypt package
        """
        logging.info("starting veracrypt download")
        self.download("https://launchpad.net/veracrypt/trunk/{0}/+download/veracrypt-{0}-setup.tar.bz2"
                 .format(VERACRYPT_VERSION), "veracrypt.tar.bz2")
        self.download("https://launchpad.net/veracrypt/trunk/{0}/+download/veracrypt-{0}-setup.tar.bz2.sig"
                 .format(VERACRYPT_VERSION), "veracrypt.tar.bz2.sig")
        logging.info("Veracrypt have been downloaded")
        logging.info("Verifying veracrypt package")
        return self.verify_veracrypt("/tmp/veracrypt.tar.bz2", "/tmp/veracrypt.tar.bz2.sig")

    @staticmethod
    def install_veracrypt():
        """
        Unzip and install veracrypt with GUI
        """
        os.system("mkdir /tmp/veracrypt")
        os.system("tar jxf /tmp/veracrypt.tar.bz2 -C /tmp/veracrypt")
        os.system("sudo -S apt install xterm")  # seems like only xterm can deal with veracrypt installer properly
        logging.info("installed xterm for veracrypt")
        os.system("/tmp/veracrypt/veracrypt-{0}-setup-gui-x64".format(VERACRYPT_VERSION))
        os.system("rm -rf /tmp/veracrypt")  # cleaning up after installation
        os.system("rm /tmp/veracrypt.*")

    @staticmethod
    def create_veracrypt_volume(default, path, password):
        """
        Creating veracrypt volume with default values or user specified ones
        :params default, path, password
        """
        try:
            logging.info("creating container at {}".format(os.path.expanduser("~")))
            if default:
                logging.info("using default settings for volume")
                os.system("xterm -e veracrypt --text --create --password {0} "
                          "--hash sha512 "
                          "--encryption AES-Twofish-Serpent "
                          "--filesystem EXT4 "
                          "--size 1000M "
                          "--volume-type normal "
                          "-k '' "
                          "--pim 0 "
                          "--random-source /dev/random {1}"
                          .format(password, path))
            else:
                logging.info("installing in fully interactive mode")
                os.system("xterm -e veracrypt --text --create "
                          "--password {0} "
                          "{1}"
                          .format(password, path))

        except Exception as e:
            logging.error("volume creation failed")
            print(e)

    @staticmethod
    def mount_veracrypt_volume():
        """
        Calls veracrypt with mount option
        """
        os.system("veracrypt --mount")

    def full_install(self, default, path, password):
        """
        Full installation of the veracrypt with volume creation
        :param default: Use default option?
        :param path: Path to the veracrypt volume
        :param password: Password for veracrypt volume
        :return:
        """
        if self.download_veracrypt():
            self.install_veracrypt()
            self.create_veracrypt_volume(default, path, password)
        else:
            os.remove("/tmp/veracrypt.tar.bz2", "/tmp/veracrypt.tar.bz2.sig")
