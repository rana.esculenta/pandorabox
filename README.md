# Pandora Project

Pandora Project is installation software for the administrators or users to make configuration of activists tools simple and fast. Right now it installs Pidgin, Veracrypt, Thunderbird and Torbrowser.

"Installation" of Torbrowser or Veracrypt means that those packages are getting download, verified and unpacked into your system.

Builds of the latest version are available at: https://we.riseup.net/herm3s/pandora-project

### Installing:

You can download the source code and run the code with python3. You would have to install pip3 and virtual environment on your sysmtem:

```bash
sudo apt install python3-pip python3-venv
```

After that it would make sense to generate virtual environment:
```bash
python3 -m venv /folder/for/virtual/env/
```
And activate it:
```bash
source /folder/for/virtual/env/bin/activate
```

Whether you decided to use virtual env or not you will have to install dependencie on your system. Navigate with terminal to the main/interface and run:
```bash
pip3 install -r requirements
```

After installation of all requirements you can run the GUI from the same terminal:
```bash
python3 main_wizard.py
```

### Compiling

You can use pyinstaller to generate binaries from the project:
```bash
pyinstaller -F main_wizard.py
```

It will generate a single binary. However there is a problem with pyinstaller that it doesn't include all the side files into binary. So you will have to copy pidgin, styles, thunderbird and Torbrowser folders into the same folder where binary is located.

### Usage
In general GUI is self explanatory. However due to heavy usage of apt you should start application from terminal.

Please report any issues found to https://0xacab.org/b8372999/pandorabox/issues