import os
import logging

class ThunderbirdInstaller(object):
    """
    Installation of thunderbird and enigmail.
    Moving thunderbird profile into veracrypt container
    """
    def __init__(self):
        self.vc_container = "/media/veracrypt1/thunderbird"
        self.vc_profile = "Thunderbird.vc"

    @staticmethod
    def thunderbird_basic_install():
        """
        Installing packages
        """
        logging.info("Installing thunderbird and enigmail")
        os.system('sudo -S apt update && sudo -S apt install thunderbird enigmail')

    @staticmethod
    def thunderbird_profile_ini():
        """
        Moving thunderbird INI profile
        """
        logging.info("Moving profiles.ini to thunderbird folder")
        os.system('cp ./thunderbird/profiles.ini ~/.thunderbird/')  # moving our own profiles.ini to .thunderbird


    @staticmethod
    def gpg_generate_key():
        """
        Generating key for the used if specified
        """
        logging.info("Generating pgp key for secure communication")
        os.system('xterm -e gpg --full-gen-key')

    def thunderbird_profile_create_vc(self):
        """
        Creating profile in VC container
        """
        logging.info("Creating thunderbird profile in veracrypt folder")
        create_profile = "thunderbird -CreateProfile \"{0} {1}\"".format(self.vc_profile, self.vc_container)
        os.system(create_profile)
